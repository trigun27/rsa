﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using Microsoft.Win32;

namespace RSA_Machine
{
    class RSACode
    {

        const int size = 500;
        Random _rand = new Random();

        List<int> _sieve = new List<int>(size);

        public long PublicKey { get; private set; }
        public long PrivateKey { get; private set; }
        public  long N { get; private set; }
        public  long Phi { get; private set; }


        public RSACode()
        {
            SieveEratosthen();
            long p = ReturnPrimeNum(20, _sieve.Count);
            long q = ReturnPrimeNum(20, _sieve.Count);

            N = p*q;

            Phi = (p - 1)*(q - 1);

            PublicKey = FindE();

            PrivateKey = ModInverse(PublicKey, Phi);

        }

        long FindE()
        {
            do
            {
                PublicKey = ReturnPrimeNum(2, _sieve.Count);
            } while (Phi % PublicKey ==0);
            return PublicKey;
        }

        public long ModInverse(long a, long n)
        {
            long i = n, v = 0, d = 1;
            while (a > 0)
            {
                long t = i / a;
                long x = a;
                a = i % x;
                i = x;
                x = d;
                d = v - t * x;
                v = x;
            }
            v %= n;
            if (v < 0) v = (v + n) % n;
            return v;
        }

        long ReturnPrimeNum(int low, int hight)
        {
            return _sieve[_rand.Next(low, hight)];
        }

        void SieveEratosthen()
        {
            int i;
            for (i = 2; i < size; i++)
                _sieve.Add(i);

            i = 0;
            var del = _sieve[i];
            while (del != _sieve.Last())
            {

                for (int j = 0; j < _sieve.Count - 1; j++)
                {
                    if ((_sieve[j] % del == 0) && (_sieve[j] != del))
                        _sieve.Remove(_sieve[j]);
                }
                i++;
                del = _sieve[i];
            }   
        }

        public void Encrypt(Stream input, Stream output)
        {
            long item;
            while ((item = input.ReadByte())>=0)
            {
                item = (long) BigInteger.ModPow(item, PublicKey, N);

                var tmp = BitConverter.GetBytes(item);
                foreach (byte t in tmp)
                    output.WriteByte(t);
            }
            output.Flush();
            output.Seek(0, SeekOrigin.Begin);
        }


        public void Decrypt(Stream input, Stream output)
        {
            int item = 0;
            var list = new byte[8];
            while ((item = input.Read(list,0,8)) > 0 )
            {
                var num = BitConverter.ToInt64(list, 0);
                num = (long)BigInteger.ModPow(num, PrivateKey, N);
                output.WriteByte((byte)num);
            }
            output.Flush();
            output.Seek(0, SeekOrigin.Begin);
        }

    }
}
