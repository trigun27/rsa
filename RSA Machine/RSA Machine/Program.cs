﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace RSA_Machine
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var mashine = new RSACode();
                using (var inputFile = File.Open(args[0], FileMode.Open))
                using (var outputFile = File.Open(args[1], FileMode.Create))
                {
                    
                    mashine.Encrypt(inputFile, outputFile);
                }
                using (var inputFile = File.Open(args[1], FileMode.Open))
                using (var outputFile = File.Open(args[2], FileMode.Create))
                {
                    mashine.Decrypt(inputFile, outputFile);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            Console.ReadLine();

        }
    }
}
