﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace DigitalSignature
{


    class CreateSignatre
    {


        public byte[] hash { get;  set; }

        public string filename { get; set; }

        public byte[] SignedHashValue { get; set; }

        public CreateSignatre(string filename)
        {
            this.filename = filename;
            
        }

        //Generate a public/private key pair.
        RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
        RSAParameters RSAKeyInfo;
        
        private void CreateHesh()
        {
            var data = File.ReadAllBytes(filename);
            var sha1 = new SHA1CryptoServiceProvider();
            hash = sha1.ComputeHash(data);
        }

        private void CreateSignDoc()
        {
            CreateHesh();
            //Create an RSAPKCS1SignatureFormatter object and pass it the 
            //RSACryptoServiceProvider to transfer the private key.
            var RSAFormatter = new RSAPKCS1SignatureFormatter(RSA);

            RSAKeyInfo = RSA.ExportParameters(false);
       
            //Set the hash algorithm to SHA1.
            RSAFormatter.SetHashAlgorithm("SHA1");

            //Create a signature for HashValue and assign it to 
            //SignedHashValue.
            SignedHashValue = RSAFormatter.CreateSignature(hash);

        }


        public void SignDoc()
        {

            CreateSignDoc();
            using (var str = File.Open("Sign_"+filename + ".txt", FileMode.Create))
            {
                str.Write(SignedHashValue, 0, SignedHashValue.Length);           
                str.Write(RSAKeyInfo.Exponent, 0, RSAKeyInfo.Exponent.Length);                            //НАДО СОЗРАНЯТЬ ПУБЛИЧНЫЙ КЛЮЧ
                str.Write(RSAKeyInfo.Modulus, 0, RSAKeyInfo.Modulus.Length);
                str.Flush();
                str.Seek(0, SeekOrigin.Begin);
            }
        }




        private void ReadSign()
        {
            using (var str = File.Open("Sign_Laytner_Warhammer_40000.txt.txt", FileMode.Open))  //"Sign_" + filename + ".txt"
            {
                str.Read(SignedHashValue, 0, 128);
                str.Read(RSAKeyInfo.Exponent, 0, 3);
                str.Read(RSAKeyInfo.Modulus, 0, 128);
                str.Flush();
                str.Seek(0, SeekOrigin.Begin);
            }
        }

        public void VerifySignature()
        {
            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
            ReadSign();
            RSA.ImportParameters(RSAKeyInfo);
            RSAPKCS1SignatureDeformatter RSADeformatter = new RSAPKCS1SignatureDeformatter(RSA);
            RSADeformatter.SetHashAlgorithm("SHA1");
            CreateHesh();
            ReadSign();

            if (RSADeformatter.VerifySignature(hash, SignedHashValue))
            {
                Console.WriteLine("The signature is valid.");
            }
            else
            {
                Console.WriteLine("The signature is not valid.");
            }

            Console.ReadLine();
        }

    }
}
