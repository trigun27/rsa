﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSignature
{
    class Program
    {
        static void Main(string[] args)
        {
            var signature = new CreateSignatre(args[0]);

            if (args[1] == "1")
                signature.SignDoc();
            else
                signature.VerifySignature();
        }
    }
}
